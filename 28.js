const level = require('level')
const readline = require('readline');
const fs = require('fs');
const db = level('dados');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
var nome = process.argv;

validaNome();


function validaNome(){
	if(nome[2] == undefined){
		rl.question('Insira o nome do arquivo:', (arq) => {
			if(arq == ""){
				validaNome();
			}
			else{
				salvar(arq)
			}
				
		});
	}else{
		salvar(nome[2]);
	}
}

function salvar(arq){
	fs.readFile(arq, "utf8" ,function read(err, data) {
		if (err) {
			throw err;
		}
		let json = JSON.parse(data);
		for (var [key, value] of Object.entries(json)) {
			db.put(key, value, function(err){
				if(err) return console.log('Erro')
			});
		}
		console.log('Salvo');
	});
}