const readline = require('readline');
var fs = require('fs');

var index = 0;
var palavras = new Map();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

ler();

function continuar(){
	process.stdout.write('\033c');
	rl.question('Deseja continuar inserindo palavras?\n1-Sim\n2-Não\n', (op) => {
		if(op == 1){
			ler();
		}
		else if(op == 2){
			process.stdout.write('\033c');
			fs.writeFile('dados.md', ''+ JSON.stringify(Object.fromEntries(palavras)) , function (err) {
			if (err){
				throw err;
			}
			else{
				console.log('Salvo!');
			}
			});
		}
		else{
			continuar();
		}
	});
}

function ler(){
	process.stdout.write('\033c');
	rl.question('Insira uma Palavra:', (pal) => {
		if(pal != ""){
			if(!palavras.has(pal)){
				palavras.set(pal,index);
				index ++;
				continuar();
			}
		}
		else{
			ler()
		}
	});
}


/*
		
	leitor.question('Insira a palavra: ', (answer) => {
		palavras.push({
			key:index,
			value:answer
		})
		index ++;
		leitor.close();
	});
}*/



